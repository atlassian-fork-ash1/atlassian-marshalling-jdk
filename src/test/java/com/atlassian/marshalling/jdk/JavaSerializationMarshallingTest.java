package com.atlassian.marshalling.jdk;

import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class JavaSerializationMarshallingTest {

    @Test(expected = MarshallingException.class)
    public void testClassCast() throws MarshallingException {
        final MarshallingPair<String> smp = JavaSerializationMarshalling.pair(String.class);

        final byte[] strData = smp.getMarshaller().marshallToBytes("Art Vs Science");
        final String revived = smp.getUnmarshaller().unmarshallFrom(strData);

        assertThat(revived, is("Art Vs Science"));

        final MarshallingPair<Long> lmp = JavaSerializationMarshalling.pair(Long.class);
        lmp.getUnmarshaller().unmarshallFrom(strData);
    }

    @Test
    public void other_classloader() throws MarshallingException {
        final MarshallingPair<Long> lmp =
                JavaSerializationMarshalling.pair(Long.class, Thread.currentThread().getContextClassLoader());

        final byte[] longData = lmp.getMarshaller().marshallToBytes(666L);

        assertThat(longData, notNullValue());

        final Long revived = lmp.getUnmarshaller().unmarshallFrom(longData);

        assertThat(revived, is(666L));
    }
}
