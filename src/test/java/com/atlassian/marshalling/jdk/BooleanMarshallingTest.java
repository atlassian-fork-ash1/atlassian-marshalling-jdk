package com.atlassian.marshalling.jdk;

import com.atlassian.marshalling.api.MarshallingPair;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BooleanMarshallingTest {
    @Test
    public void testTrue() throws Exception {
        final MarshallingPair<Boolean> smp = BooleanMarshalling.pair();

        final byte[] data = smp.getMarshaller().marshallToBytes(true);
        final boolean revived = smp.getUnmarshaller().unmarshallFrom(data);

        assertThat(revived, is(true));
    }

    @Test
    public void testFalse() throws Exception {
        final MarshallingPair<Boolean> smp = BooleanMarshalling.pair();

        final byte[] data = smp.getMarshaller().marshallToBytes(false);
        final boolean revived = smp.getUnmarshaller().unmarshallFrom(data);

        assertThat(revived, is(false));
    }

    @Test(expected=NullPointerException.class)
    public void testNull() throws Exception {
        final MarshallingPair<Boolean> smp = BooleanMarshalling.pair();

        smp.getMarshaller().marshallToBytes(null);
    }
}
