package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;

/**
 * Implementation for boolean values
 *
 * @since 1.1.0
 */
@PublicApi
public class BooleanMarshalling implements Marshaller<Boolean>, Unmarshaller<Boolean> {

    /**
     * @return a {@link MarshallingPair}.
     */
    public static MarshallingPair<Boolean> pair() {
        final BooleanMarshalling bm = new BooleanMarshalling();
        return new MarshallingPair<>(bm, bm);
    }

    @Override
    public byte[] marshallToBytes(Boolean aBoolean) throws MarshallingException {
        return new byte[]{(byte) (aBoolean ? 1 : 0)};
    }

    @Override
    public Boolean unmarshallFrom(byte[] bytes) throws MarshallingException {
        return bytes[0] == 1;
    }
}
