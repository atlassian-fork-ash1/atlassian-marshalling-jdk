package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Optimised implementation for {@link Serializable} objects.
 *
 * @param <T> the type being marshalled
 * @since 1.0.0
 */
@PublicApi
public class JavaSerializationMarshalling<T extends Serializable> implements Marshaller<T>, Unmarshaller<T> {
    private final Class<T> clazz;
    private final Optional<ClassLoader> loader;

    /**
     * Creates an instance that uses the default implementation of
     * {@link java.io.ObjectInputStream#resolveClass(ObjectStreamClass)} to resolve {@link Class} objects.
     *
     * @param clazz used to verify the unmarshalled class is of the correct type
     */
    public JavaSerializationMarshalling(Class<T> clazz) {
        this.clazz = requireNonNull(clazz);
        this.loader = Optional.empty();
    }

    /**
     * Creates an instance that uses the supplied {@link ClassLoader} to resolve
     * {@link Class} objects.
     *
     * @param clazz  used to verify the unmarshalled class is of the correct type
     * @param loader used to resolve classes when unmarshalling
     */
    public JavaSerializationMarshalling(Class<T> clazz, ClassLoader loader) {
        this.clazz = requireNonNull(clazz);
        this.loader = Optional.of(loader);
    }

    @Override
    public byte[] marshallToBytes(T obj) throws MarshallingException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(obj);
        } catch (IOException ioe) {
            throw new MarshallingException("Unable to marshall", ioe);
        }

        return baos.toByteArray();
    }

    @Override
    public T unmarshallFrom(byte[] raw) throws MarshallingException {
        final ByteArrayInputStream bais = new ByteArrayInputStream(raw);

        try (ObjectInputStream ois = createObjectInputStream(bais)) {
            return clazz.cast(ois.readObject());
        } catch (ClassCastException | ClassNotFoundException | IOException ex) {
            throw new MarshallingException("Unable to unmarshall", ex);
        }
    }

    private ObjectInputStream createObjectInputStream(InputStream istr) throws IOException {
        return loader.isPresent()
                ? new ObjectInputStreamWithLoader(istr, loader.get())
                : new ObjectInputStream(istr);
    }

    private static class ObjectInputStreamWithLoader extends ObjectInputStream {
        private final ClassLoader loader;

        public ObjectInputStreamWithLoader(InputStream in, ClassLoader loader) throws IOException {
            super(in);
            this.loader = requireNonNull(loader);
        }

        @Override
        protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
            return Class.forName(desc.getName(), false, loader);
        }
    }

    /**
     * Creates a {@link MarshallingPair}
     *
     * @param clazz used to verify the unmarshalled class is of the correct type
     * @param <T>   the type being marshalled
     * @return a {@link MarshallingPair}
     */
    public static <T extends Serializable> MarshallingPair<T> pair(Class<T> clazz) {
        final JavaSerializationMarshalling<T> jsm = new JavaSerializationMarshalling<>(clazz);
        return new MarshallingPair<>(jsm, jsm);
    }

    /**
     * Creates a {@link MarshallingPair}
     *
     * @param clazz  used to verify the unmarshalled class is of the correct type
     * @param loader used to resolve classes when unmarshalling
     * @param <T>    the type being marshalled
     * @return a {@link MarshallingPair}
     */
    public static <T extends Serializable> MarshallingPair<T> pair(Class<T> clazz, ClassLoader loader) {
        final JavaSerializationMarshalling<T> jsm = new JavaSerializationMarshalling<>(clazz, loader);
        return new MarshallingPair<>(jsm, jsm);
    }
}
